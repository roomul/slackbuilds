#!/bin/sh -e

APP="sxhkd"
VERSION="${VERSION:-0.6.2}"
RELEASE="1"
BUILD="${BUILD:-all}"

SOURCE="${SOURCE:-https://github.com/baskerville/sxhkd/archive/refs/tags/$VERSION.tar.gz}"

CWD=$(pwd)
PKG=$TMP/pkg

if [ "$TMP" = "" ]; then
  TMP=`mktemp -u` || TMP=/tmp
fi

if [ -z "$ARCH" ]; then
 case "$( uname -m )" in
   i?86) ARCH=i486 ;;
   arm*) ARCH=arm ;;
   # Unless $ARCH is already set, use uname -m for all other archs:
      *) ARCH=$( uname -m ) ;;
 esac
fi

if [ "`which wget`" != "" ]; then
	if [ ! -f "`basename $SOURCE`" ]; then
		wget -c $SOURCE -P $CWD/
	fi
fi

##
install -dm755 $TMP $PKG
cd $TMP || exit 1
tar -xvf $CWD/$VERSION.tar.gz || exit 1
cd $APP-$VERSION || exit 1

chown -R root:root .
chmod -R u+w,go+r-w,a-s .

make PREFIX=/usr
make PREFIX=/usr DESTDIR="$PKG" install || exit 1

mkdir -p $PKG/usr/doc/$APP-$VERSION
cp LICENSE $PKG/usr/doc/$APP-$VERSION/
find $PKG/usr/doc/$APP-$VERSION -type f -exec chmod 644 {} \;

cat $CWD/$APP.SlackBuild > $PKG/usr/doc/$APP-$VERSION/$APP.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

# Add doinst.sh to package (if it exists)
if [ -e $CWD/doinst.sh.gz ]; then
  zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh
fi

# Strip some libraries and binaries
( cd $PKG
   find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
   find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)

# Compress man pages if they exist
if [ -d $PKG/usr/share/man ]; then
  ( cd $PKG/usr/share/man
  find . -type f -exec gzip -9 {} \;
  for i in $(find . -type l) ; do ln -s $(readlink $i).gz $i.gz ; rm $i ; done
  ) 
fi

# Build the package
cd $PKG
/sbin/makepkg -l y -c n $CWD/${APP}-${VERSION}-${ARCH}-${RELEASE}_${BUILD}.txz

# clean
sync
rm -rf $PKG $TMP
unset PKG TMP

